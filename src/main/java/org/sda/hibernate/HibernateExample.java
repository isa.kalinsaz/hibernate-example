package org.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.hibernate.config.HibernateConfig;
import org.sda.hibernate.entity.BankEntity;
import org.sda.hibernate.entity.CustomerEntity;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

public class HibernateExample {

    public static void main(String[] args) {

        final SessionFactory sessionFactory = HibernateConfig.buildSessionFactory();

        final Session session = sessionFactory.openSession();

        /*session.beginTransaction();

        CustomerEntity newCustomerEntity = CustomerEntity.builder().name("John").surname("Snow")
                .dateOfBirth(Date.valueOf(LocalDate.of(2000, 1, 1)))
                .monthlyIncome(BigDecimal.valueOf(2000))
                .bankId(1)
                .identityId(null)
                .build();

        session.save(newCustomerEntity);

        session.getTransaction().commit();*/

        session.createQuery("from CustomerEntity", CustomerEntity.class).list().forEach(System.out::println);
        session.createQuery("from BankEntity ", BankEntity.class).list().forEach(System.out::println);

        final CustomerEntity customerEntity = session.getReference(CustomerEntity.class, 1);
        final CustomerEntity customerEntity2 = session.find(CustomerEntity.class, 2);
        System.out.println("Customer entity with Id : 1 = " + customerEntity);
        System.out.println("Customer entity with Id : 2 = " + customerEntity2);

        if (customerEntity != null) {
            session.getTransaction().begin();
            customerEntity.setName("Isa");
            customerEntity.setSurname("Kalinsaz");
            customerEntity.setMonthlyIncome(BigDecimal.valueOf(2500));
            customerEntity.setDateOfBirth(Date.valueOf(LocalDate.of(2000, 2, 1)));
            customerEntity.getBank().setName("SEB BANK NEW TEST");
            session.save(customerEntity);
            session.getTransaction().commit();
        }

        /*
        if (customerEntity2 != null) {
            session.getTransaction().begin();
            session.delete(customerEntity2);
            session.getTransaction().commit();
        }*/

        session.close();


    }
}
