package org.sda.hibernate.config;


import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.sda.hibernate.entity.AbstractBaseEntity;
import org.sda.hibernate.entity.AccountEntity;
import org.sda.hibernate.entity.BankEntity;
import org.sda.hibernate.entity.CurrencyEntity;
import org.sda.hibernate.entity.CustomerEntity;
import org.sda.hibernate.entity.IdentityEntity;

public class HibernateConfig {

    public static SessionFactory buildSessionFactory() {
        try {
            final Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();

            configuration.addAnnotatedClass(AbstractBaseEntity.class);
            configuration.addAnnotatedClass(AccountEntity.class);
            configuration.addAnnotatedClass(CustomerEntity.class);
            configuration.addAnnotatedClass(BankEntity.class);
            configuration.addAnnotatedClass(IdentityEntity.class);
            configuration.addAnnotatedClass(CurrencyEntity.class);

            return configuration.buildSessionFactory(serviceRegistry);

        } catch (Exception ex) {
            System.err.println("Initial SessionFactory creation failed." + ex.getMessage());
            throw ex;
        }
    }
}
