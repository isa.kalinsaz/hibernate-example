package org.sda.hibernate.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account")
@ToString(callSuper = true)
public class AccountEntity extends AbstractBaseEntity {

    @Column
    private String accountNumber;

    @Column
    private String accountName;

    @Column
    private BigDecimal accountBalance;

    @Column
    private Boolean isActive;

    @ToString.Exclude
    @Fetch(FetchMode.JOIN)
    @ManyToOne(targetEntity = CustomerEntity.class)
    @JoinColumn(name = "customerId", insertable = false, updatable = false)
    private CustomerEntity customer;

    @ManyToMany
    @Fetch(FetchMode.JOIN)
    @JoinTable(name = "account_currency",
            joinColumns = {@JoinColumn(name = "accountId")},
            inverseJoinColumns = {@JoinColumn(name = "currencyId")})
    private List<CurrencyEntity> currencies;

}
