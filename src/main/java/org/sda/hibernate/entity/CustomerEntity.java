package org.sda.hibernate.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer")
@ToString(callSuper = true)
public class CustomerEntity extends AbstractBaseEntity {

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private Date dateOfBirth;

    @Column
    private BigDecimal monthlyIncome;

    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "customer")
    private List<AccountEntity> accountEntities;

    @ToString.Exclude
    @Fetch(FetchMode.JOIN)
    @ManyToOne(targetEntity = BankEntity.class)
    @JoinColumn(name = "bankId", insertable = false, updatable = false)
    private BankEntity bank;

    @Fetch(FetchMode.JOIN)
    @OneToOne(mappedBy = "customer")
    private IdentityEntity identity;

}




