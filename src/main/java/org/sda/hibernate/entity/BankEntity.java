package org.sda.hibernate.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bank")
@ToString(callSuper = true)
public class BankEntity extends AbstractBaseEntity {

    @Column
    private String name;

    @Column
    private String country;

    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "bank", cascade = CascadeType.REMOVE)
    private List<CustomerEntity> customers;

}
