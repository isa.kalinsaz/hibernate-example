package org.sda.hibernate.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "identity")
@ToString(callSuper = true)
public class IdentityEntity extends AbstractBaseEntity {

    @Column
    private String documentId;

    @Column
    private String issuedBy;

    @Column
    private Date issuedDate;

    @Column
    private String issuedCountry;

    @Column
    private Date validUntil;

    @Column
    private Integer customerId;

    @ToString.Exclude
    @Fetch(FetchMode.JOIN)
    @OneToOne(targetEntity = CustomerEntity.class)
    @JoinColumn(name = "customerId", insertable = false, updatable = false)
    private CustomerEntity customer;

}
