package org.sda.hibernate.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currency")
@ToString(callSuper = true)
public class CurrencyEntity extends AbstractBaseEntity {

    @Column
    private String name;

    @Column
    private String code;

    @Fetch(FetchMode.JOIN)
    @ToString.Exclude
    @ManyToMany(mappedBy = "currencies")
    private List<AccountEntity> accounts;

}
